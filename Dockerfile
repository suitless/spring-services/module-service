FROM openjdk:11
COPY /build/libs/nl.suitless.module-service-0.0.1-SNAPSHOT.jar run.jar
EXPOSE 3304
CMD "java" "-jar" "run.jar"
