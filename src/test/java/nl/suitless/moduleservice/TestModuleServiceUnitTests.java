package nl.suitless.moduleservice;

import nl.suitless.moduleservice.Data.testRepositories.IModuleTestRepository;
import nl.suitless.moduleservice.Domain.Entities.Module;
import nl.suitless.moduleservice.Domain.Entities.Node;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleAlreadyExistException;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleNotFoundException;
import nl.suitless.moduleservice.Services.Module.TestModuleService;
import org.bson.Document;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestModuleServiceUnitTests {

    private Module module;

    @Mock
    private IModuleTestRepository moduleRepository;

    @InjectMocks
    private TestModuleService moduleService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node(50, 0, new Document()));
        nodes.add(new Node(50, 0, new Document()));

        module = new Module("IP", "Description", "disclaimer", "pdfDisclaimer", 1, 0, "URI", new ArrayList<>(), nodes, true, "Suitless");
    }

    @Test
    public void findModuleByIDExists() {
        when(moduleRepository.findById(anyString())).thenReturn(Optional.of(module));

        var foundModule = moduleService.getModuleById("1-2-4");

        verify(moduleRepository, times(1)).findById("1-2-4");
        Assert.assertEquals(module, foundModule);
    }

    @Test(expected = ModuleNotFoundException.class)
    public void findModuleByIDDoesNotExist() {
        when(moduleRepository.findById(anyString())).thenReturn(Optional.empty());

        moduleService.getModuleById("test");
    }

    @Test
    public void createModuleValid() {
        when(moduleRepository.findByName(anyString())).thenReturn(Optional.empty());
        when(moduleRepository.save(any(Module.class))).thenReturn(module);

        var createdModule = moduleService.createModule("test", "description", "disclaimer", "pdfDisclaimer", 1, 0, "URI", new ArrayList<>(),
                Collections.emptyList(), true, "suitless");

        verify(moduleRepository, times(1)).save(any(Module.class));
        Assert.assertEquals(createdModule, module);

    }

    @Test(expected = ModuleAlreadyExistException.class)
    public void createModuleAlreadyExists() {
        when(moduleRepository.findByName(anyString())).thenReturn(Optional.of(module));

        moduleService.createModule("test", "description", "disclaimer", "pdfDisclaimer", 1, 0, "URI", new ArrayList<>(), Collections.emptyList(), true, "suitless");

    }
}
