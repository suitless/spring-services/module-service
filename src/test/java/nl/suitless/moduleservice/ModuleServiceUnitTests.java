package nl.suitless.moduleservice;

import nl.suitless.moduleservice.Config.BinConfig.BinConfig;
import nl.suitless.moduleservice.Data.Repositories.IBinModuleRepository;
import nl.suitless.moduleservice.Data.Repositories.IModuleRepository;
import nl.suitless.moduleservice.Domain.Entities.*;
import nl.suitless.moduleservice.Domain.Entities.Module;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleNotFoundException;
import nl.suitless.moduleservice.Services.Authorization.IAuthorizationService;
import nl.suitless.moduleservice.Services.Module.ModuleService;
import org.bson.Document;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ModuleServiceUnitTests {
	private Module module;
	private Module latestModule;
	private Module nonExposedModule;
	private Page<ModuleReadOnly> moduleReadOnlyPage;
	private List<Module> moduleList;

	private AuthorizationConfig authConfig;
	private Collection<GrantedAuthority> authorities;
	private String jwt;

	@Mock
	private IModuleRepository moduleRepository;
	@Mock
	private IBinModuleRepository binModuleRepository;
	@Mock
	private IAuthorizationService authorizationService;
	@Mock
	private BinConfig binConfig;

	@InjectMocks
	private ModuleService moduleService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		List<Node> nodes = new ArrayList<>();
		nodes.add(new Node(50, 0, new Document()));
		nodes.add(new Node(50, 0, new Document()));

		module = new Module("IP", "Descriptie", "disclaimer", "pdfDisclaimer", 1, 0, "URI", new ArrayList<>(), nodes, true, "Suitless");
		latestModule = new Module("IP", "Descriptie", "disclaimer", "pdfDisclaimer", 2, 0, "URI", new ArrayList<>(), nodes, true, "Suitless");
		nonExposedModule = new Module("IP", "Descriptie", "disclaimer", "pdfDisclaimer", 2, 0, "URI", new ArrayList<>(), nodes, false, "Suitless");
		var anotherModule = new Module("Privacy", "Descriptie", "disclaimer", "pdfDisclaimer", 1, 0, "URI", new ArrayList<>(), nodes, true, "Suitless");

		moduleList = new ArrayList<>();
		moduleList.add(module);
		moduleList.add(latestModule);
		moduleList.add(anotherModule);

		var moduleReadOnlyList = new ArrayList<ModuleReadOnly>();
		moduleReadOnlyList.add(new ModuleReadOnly(latestModule.getName(), latestModule.getDescription(), latestModule.getDisclaimer(),  latestModule.getPdfDisclaimer(),
				latestModule.getVersion(), latestModule.getEditorVersion(), latestModule.getImage(), latestModule.getCreatedAt(), latestModule.getTags(), latestModule.isExposed(),
				latestModule.getOwner()));
		moduleReadOnlyList.add(new ModuleReadOnly(module.getName(), module.getDescription(), module.getDisclaimer(),  module.getPdfDisclaimer(),
				module.getVersion(), latestModule.getEditorVersion(), module.getImage(), module.getCreatedAt(), module.getTags(), module.isExposed(), module.getOwner()));
		moduleReadOnlyList.add(new ModuleReadOnly(anotherModule.getName(), anotherModule.getDescription(), anotherModule.getDisclaimer(),  anotherModule.getPdfDisclaimer(),
				anotherModule.getVersion(), latestModule.getEditorVersion(), anotherModule.getImage(), anotherModule.getCreatedAt(), anotherModule.getTags(), anotherModule.isExposed(), anotherModule.getOwner()));

		moduleReadOnlyPage = new PageImpl<>(moduleReadOnlyList);

		authorities = new ArrayList<>();
		((ArrayList<GrantedAuthority>) authorities).add(0, new SimpleGrantedAuthority("editor"));
		jwt = "jwt-token shit";
		authConfig = mock(AuthorizationConfig.class, RETURNS_DEEP_STUBS);
	}

	@Test
	public void getModuleByIdExists() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.of(module));
		when(authConfig.getAuthorities()).thenReturn(authorities);

		var foundModule = moduleService.getModuleById("1-2-4", authConfig);

		verify(moduleRepository, times(1)).findById("1-2-4");
		Assert.assertEquals(module, foundModule);
	}

	@Test(expected = ModuleNotFoundException.class)
	public void getModulesByIdNotFound() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.empty());
		when(authConfig.getAuthorities()).thenReturn(authorities);

		moduleService.getModuleById("yeet", authConfig);
	}

	@Test
	public void getLatestNonExposedModuleByIdExists() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.of(module));
		when(moduleRepository.findFirstByNameAndOwnerAndExposedIsTrueOrderByVersionDesc(anyString(), anyString())).thenReturn(Optional.of(latestModule));

		var foundModule = moduleService.getLatestNonExposedModuleById("1-2-4");

		verify(moduleRepository, times(1)).findById("1-2-4");
		verify(moduleRepository, times(1)).findFirstByNameAndOwnerAndExposedIsTrueOrderByVersionDesc(module.getName(), module.getOwner());
		Assert.assertEquals(latestModule, foundModule);
	}

	@Test(expected = ModuleNotFoundException.class)
	public void getLatestNonExposedModuleByIdNotFound1() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.empty());

		moduleService.getLatestNonExposedModuleById("1-2-4");
	}

	@Test(expected = ModuleNotFoundException.class)
	public void getLatestNonExposedModuleByIdNotFound2() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.of(module));
		when(moduleRepository.findFirstByNameAndOwnerAndExposedIsTrueOrderByVersionDesc(anyString(), anyString())).thenReturn(Optional.empty());

		moduleService.getLatestNonExposedModuleById("1-2-4");
	}

	@Test
	public void getModuleByNameValid(){
		when(moduleRepository.findByNameAndVersionAndOwner(anyString(), anyInt(), anyString())).thenReturn(Optional.of(module));
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var foundModule = moduleService.getModuleByName("IP", 1, "Suitless", authConfig);

		verify(moduleRepository, times(1)).findByNameAndVersionAndOwner("IP", 1, "Suitless");
		Assert.assertEquals(module, foundModule);
	}

	@Test(expected = ModuleNotFoundException.class)
	public void getModuleByNameNotFound() {
		when(moduleRepository.findByNameAndVersionAndOwner(anyString(), anyInt(), anyString())).thenReturn(Optional.empty());
		when(authConfig.getAuthorities()).thenReturn(authorities);

		moduleService.getModuleByName("yeet", 1, "Suitless", authConfig);
	}

	@Test
	public void getLatestModuleByNameValid() {
		when(moduleRepository.findByNameAndOwnerOrderByCreatedAtDesc(anyString(), anyString())).thenReturn(Optional.of(latestModule));
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var foundModule = moduleService.getLatestModuleByName("IP", "Suitless", authConfig);

		verify(moduleRepository, times(1)).findByNameAndOwnerOrderByCreatedAtDesc("IP", "Suitless");
		Assert.assertEquals(latestModule.getId(), foundModule.getId());
	}

	@Test(expected = ModuleNotFoundException.class)
	public void getLatestModuleByNameNotExist() {
		when(authConfig.getAuthorities()).thenReturn(authorities);

		moduleService.getLatestModuleByName("yeet", "Suitless", authConfig);
	}

	@Test
	public void getModulesValid() {
		when(moduleRepository.findByVersionAndOwnerAndExposedIsTrueOrderByVersionDesc(anyInt(), anyString(), any())).thenReturn(moduleReadOnlyPage);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		Page<ModuleReadOnly> foundModules = moduleService.getModules(1, false, "Suitless",1, 2, authConfig);

		Pageable paging = PageRequest.of(1, 2);
		verify(moduleRepository, times(1)).findByVersionAndOwnerAndExposedIsTrueOrderByVersionDesc(1, "Suitless", paging);
		Assert.assertEquals(foundModules.getContent().size(), 3);
	}

	/**
	 * Tests for getAllLatestModules are not relevant
	 * The method only calls other methods.
	 */

	@Test
	public void getLatestModulesValid() {
		when(moduleRepository.findLatestModulesPrivateByOwner(anyString(), any())).thenReturn(moduleReadOnlyPage);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		Page<ModuleReadOnly> foundModules = moduleService.getLatestModules(true, "Suitless", 0, 2, authConfig);

		Pageable paging = PageRequest.of(0, 2);
		verify(moduleRepository, times(1)).findLatestModulesPrivateByOwner("Suitless", paging);
		Assert.assertEquals(foundModules.getContent().size(), 3);
	}

	@Test
	public void getModulesByNameValidPublic() {
		when(moduleRepository.findByNameAndOwnerAndExposedIsTrueOrderByCreatedAtDesc(anyString(), anyString(), any())).thenReturn(moduleReadOnlyPage);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		Page<ModuleReadOnly> foundModules = moduleService.getModulesByName(false, "Suitless", "yeet", 1, 3, authConfig);

		Pageable paging = PageRequest.of(1, 3);
		verify(moduleRepository, times(1)).findByNameAndOwnerAndExposedIsTrueOrderByCreatedAtDesc("yeet", "Suitless", paging);
		Assert.assertEquals(foundModules.getContent().size(), 3);
	}

	@Test
	public void getModulesByNameValidPrivateAndPublic() {
		when(moduleRepository.findByNameAndOwnerOrderByCreatedAtDesc(anyString(), anyString(), any())).thenReturn(moduleReadOnlyPage);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		Page<ModuleReadOnly> foundModules = moduleService.getModulesByName(true, "Suitless", "yeet",1 ,3, authConfig);

		Pageable paging = PageRequest.of(1, 3);
		verify(moduleRepository, times(1)).findByNameAndOwnerOrderByCreatedAtDesc("yeet", "Suitless", paging);
		Assert.assertEquals(foundModules.getContent().size(), 3);
	}

	@Test
	public void getAllModulesByNameValid() {
		when(moduleRepository.findByNameOrderByCreatedAtDesc(anyString(), any())).thenReturn(moduleReadOnlyPage);
		when(authConfig.getAuthorities()).thenReturn(authorities);

		Page<ModuleReadOnly> foundModules = moduleService.getAllModulesByName("test",1, 3, authConfig);

		Pageable paging = PageRequest.of(1, 3);
		verify(moduleRepository, times(1)).findByNameOrderByCreatedAtDesc("test", paging);
		Assert.assertEquals(foundModules.getContent().size(), 3);
	}

	@Test
	public void createModuleValid() {
		when(moduleRepository.save(any(Module.class))).thenReturn(module);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var createdModule = moduleService.createModule("hoi", "descriptie", "disclaimer", "pdfDisclaimer", 1, 0, "URI", new ArrayList<>(), Collections.emptyList(), true, "Suitless", authConfig);

		verify(moduleRepository, times(1)).save(any(Module.class));
		Assert.assertEquals(createdModule, module);
	}

	@Test
	public void updateModuleValid() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.of(module));
		when(moduleRepository.save(any(Module.class))).thenReturn(module);
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		var updatedModule = moduleService.updateModule("1-2-4", "IP", "descriptie", "disclaimer", "pdfDisclaimer", 2, 0, "URI", Collections.emptyList(),
				Collections.emptyList(), false, "Suitless", authConfig);

		verify(moduleRepository, times(1)).findById("1-2-4");
		verify(moduleRepository, times(1)).save(any(Module.class));
		Assert.assertEquals(0, updatedModule.getNodes().size());
	}

	@Test(expected = ModuleNotFoundException.class)
	public void updateModuleNotExist() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.empty());
		when(authConfig.getAuthorities()).thenReturn(authorities);
		when(authConfig.getJwtToken()).thenReturn(jwt);

		moduleService.updateModule("1-2-4","yeet", "descriptie", "disclaimer", "pdfDisclaimer", 2, 0, "URI", Collections.emptyList(),
				Collections.emptyList(), true, "Suitless", authConfig);
	}

	@Test
	public void deleteModuleValid() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.of(module));
		when(binConfig.getDaysExpired()).thenReturn(10);
		when(authConfig.getAuthorities()).thenReturn(authorities);

		moduleService.deleteModule("1-2-4", authConfig);

		verify(moduleRepository, times(1)).findById("1-2-4");
		verify(binModuleRepository, times(1)).save(any(BinModule.class));
		verify(moduleRepository, times(1)).delete(any(Module.class));
	}

	@Test(expected = ModuleNotFoundException.class)
	public void deleteModuleNotFound() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.empty());
		when(authConfig.getAuthorities()).thenReturn(authorities);

		moduleService.deleteModule("1-2-4", authConfig);
	}

	@Test
	public void restoreModuleValid() {
		when(binModuleRepository.findById(anyString())).thenReturn(Optional.of(new BinModule(module, new Date())));
		when(authConfig.getAuthorities()).thenReturn(authorities);

		moduleService.restoreModule("1-2-3-4", authConfig);

		verify(binModuleRepository, times(1)).findById("1-2-3-4");
		verify(binModuleRepository, times(1)).delete(any(BinModule.class));
		verify(moduleRepository, times(1)).save(any(Module.class));
	}

	@Test(expected = ModuleNotFoundException.class)
	public void restoreModuleNotFound() {
		when(binModuleRepository.findById(anyString())).thenReturn(Optional.empty());
		when(authConfig.getAuthorities()).thenReturn(authorities);

		moduleService.restoreModule("1-2-3-4", authConfig);
	}

	@Test
	public void moduleExistsValidTrue() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.of(module));
		when(authConfig.getAuthorities()).thenReturn(authorities);

		var moduleExists = moduleService.moduleExists("1-2-4", authConfig);

		verify(moduleRepository, times(1)).findById("1-2-4");
		Assert.assertTrue(moduleExists);
	}

	@Test
	public void moduleExistsValidFalse() {
		when(moduleRepository.findById(anyString())).thenReturn(Optional.empty());
		when(authConfig.getAuthorities()).thenReturn(authorities);

		var moduleExists = moduleService.moduleExists("1-2-4", authConfig);

		verify(moduleRepository, times(1)).findById("1-2-4");
		Assert.assertFalse(moduleExists);
	}
}

