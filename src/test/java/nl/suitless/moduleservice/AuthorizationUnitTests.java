package nl.suitless.moduleservice;

import nl.suitless.moduleservice.Config.RoleManagement.RoleManagementConfig;
import nl.suitless.moduleservice.Domain.Exceptions.UnauthorizedException;
import nl.suitless.moduleservice.Services.Authorization.AuthorizationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationUnitTests {

    private Collection<GrantedAuthority> authorities;

    @Mock
    private RoleManagementConfig roleManagementConfig;
    @Mock
    RestTemplateBuilder restTemplateBuilder;
    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private AuthorizationService authorizationService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        authorities = new ArrayList<>();
        ((ArrayList<GrantedAuthority>) authorities).add(0, new SimpleGrantedAuthority("admin"));
    }

    @Test
    public void checkValid() {
        when(roleManagementConfig.getRoles()).thenReturn(new String[]{"admin", "editor"});

        authorizationService.check(authorities);

        verify(roleManagementConfig, times(1)).getRoles();
    }

    @Test(expected = UnauthorizedException.class)
    public void checkInvalid() {
        when(roleManagementConfig.getRoles()).thenReturn(new String[]{"user"});

        authorizationService.check(authorities);
    }

    @Test()
    public void checkAdminOnlyValid() {
        when(roleManagementConfig.getAdminRole()).thenReturn("admin");

        authorizationService.checkAdminOnly(authorities);

        verify(roleManagementConfig, times(1)).getAdminRole();
    }

    @Test(expected = UnauthorizedException.class)
    public void checkAdminOnlyInvalid() {
        when(roleManagementConfig.getAdminRole()).thenReturn("user");

        authorizationService.checkAdminOnly(authorities);
    }

    @Test()
    public void checkUserInModuleBusinessValid() {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), any(Class.class)))
                .thenReturn(new ResponseEntity<Boolean>(true, HttpStatus.OK));

        authorizationService.checkUserInModuleBusiness("jwt shit", "046e7991-8e94-48e8-9dfb-5beb54306793");
    }

    @Test(expected = UnauthorizedException.class)
    public void checkUserInModuleBusinessInvalid() {
        when(restTemplate.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), any(Class.class)))
                .thenReturn(new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND));

        authorizationService.checkUserInModuleBusiness("jwt shit", "046e7991-8e94-48e8-9dfb-5beb54306793");
    }
}
