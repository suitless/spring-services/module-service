package nl.suitless.moduleservice.Domain.Entities;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.BasicDBObject;
import org.bson.Document;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Node {
    private int id;
    private int type;
    private Document typeData;

    public Node() {
    }

    public Node(int id, int type, Document typeData) {
        this.id = id;
        this.type = type;
        this.typeData = typeData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Document getTypeData() {
        return typeData;
    }

    @JsonSetter("typeData")
    public void setTypeData(JsonNode typeData) {
        this.typeData = new Document(BasicDBObject.parse(typeData.toString()).toMap());
    }

    public void setTypeData(Document typeData) {
        this.typeData = typeData;
    }
}
