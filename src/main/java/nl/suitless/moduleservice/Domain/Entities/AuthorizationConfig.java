package nl.suitless.moduleservice.Domain.Entities;

import nl.suitless.moduleservice.Domain.Exceptions.UnsupportedTokenException;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.security.Principal;
import java.util.Collection;

public class AuthorizationConfig {
    Principal principal;

    public AuthorizationConfig(Principal principal) {
        this.principal = principal;
    }

    /**
     * To get the jwt token if you need it for orchestrational communication to another service.
     * @return a jwt token
     */
    public String getJwtToken () {
        if (principal instanceof KeycloakAuthenticationToken) {
            var keycloakPrincipal = (KeycloakPrincipal) ((KeycloakAuthenticationToken) principal).getPrincipal();
            return keycloakPrincipal.getKeycloakSecurityContext().getTokenString();
        } else {
            throw new UnsupportedTokenException("given token is not supported");
        }
    }

    /**
     * To get all the authorities of the user.
     * It needs to be keycloak authorities.
     * @return a collection of authorities
     */
    public Collection<GrantedAuthority> getAuthorities() {
        if (principal instanceof KeycloakAuthenticationToken) {
            return ((KeycloakAuthenticationToken) principal).getAuthorities();
        } else {
            throw new UnsupportedTokenException("given token is not supported");
        }
    }
}
