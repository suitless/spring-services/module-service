package nl.suitless.moduleservice.Domain.Entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
public class BinModule {
    @Id
    private String id;
    private String name;
    private String description;
    private String disclaimer;
    private String pdfDisclaimer;
    private int version;
    private int editorVersion;
    private String image;
    private Date createdAt;
    private List<String> tags = new ArrayList<>();
    private List<Node> nodes = new ArrayList<>();
    private Date expireDate;

    //Ownership and visibility properties
    private boolean exposed;
    private String owner;

    public BinModule() {
    }

    public BinModule(Module module, Date expireDate) {
        this.name = module.getName();
        this.description = module.getDescription();
        this.disclaimer = module.getDisclaimer();
        this.pdfDisclaimer = module.getPdfDisclaimer();
        this.version = module.getVersion();
        this.editorVersion = module.getEditorVersion();
        this.image = module.getImage();
        this.createdAt = module.getCreatedAt();
        this.tags = module.getTags();
        this.nodes = module.getNodes();
        this.expireDate = expireDate;
        this.exposed = module.isExposed();
        this.owner = module.getOwner();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getPdfDisclaimer() {
        return pdfDisclaimer;
    }

    public void setPdfDisclaimer(String pdfDisclaimer) {
        this.pdfDisclaimer = pdfDisclaimer;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getEditorVersion() {
        return editorVersion;
    }

    public void setEditorVersion(int editorVersion) {
        this.editorVersion = editorVersion;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public boolean isExposed() {
        return exposed;
    }

    public void setExposed(boolean exposed) {
        this.exposed = exposed;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
