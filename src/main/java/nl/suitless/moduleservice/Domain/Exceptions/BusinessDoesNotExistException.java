package nl.suitless.moduleservice.Domain.Exceptions;

public class BusinessDoesNotExistException extends RuntimeException {
    public BusinessDoesNotExistException(String exception) { super(exception); }
}
