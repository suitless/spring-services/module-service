package nl.suitless.moduleservice.Domain.Exceptions;

public class VersionConflictException extends RuntimeException{
    public VersionConflictException(String exception) { super(exception); }
}
