package nl.suitless.moduleservice.Domain.Exceptions;

public class ModulesInvalidException extends  RuntimeException {
    public ModulesInvalidException(String exception) { super(exception);}
}
