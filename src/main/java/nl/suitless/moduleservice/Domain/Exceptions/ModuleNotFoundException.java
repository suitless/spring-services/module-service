package nl.suitless.moduleservice.Domain.Exceptions;

public class ModuleNotFoundException extends RuntimeException {
    public ModuleNotFoundException(String exception) {
        super(exception);
    }
}
