package nl.suitless.moduleservice.Domain.Exceptions;

public class ModuleAlreadyExistException extends RuntimeException {
    public ModuleAlreadyExistException(String exception) {
        super(exception);
    }
}
