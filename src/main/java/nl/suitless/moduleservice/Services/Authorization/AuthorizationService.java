package nl.suitless.moduleservice.Services.Authorization;

import nl.suitless.moduleservice.Config.RoleManagement.RoleManagementConfig;
import nl.suitless.moduleservice.Domain.Exceptions.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;

@Service
public class AuthorizationService implements IAuthorizationService {
    private RoleManagementConfig roleManagementConfig;
    private RestTemplate restTemplate;

    @Autowired
    public AuthorizationService(RoleManagementConfig roleManagementConfig, RestTemplateBuilder builder) {
        this.roleManagementConfig = roleManagementConfig;
        this.restTemplate = builder.build();
    }

    @Override
    public void check(Collection<GrantedAuthority> authorities) {
        boolean isAuthorized = false;

        if(authorities != null && !authorities.isEmpty()) {
            for (GrantedAuthority authority: authorities) {
                for (String role: roleManagementConfig.getRoles()) {
                    if(authority.getAuthority().contains(role)) {
                        isAuthorized = true;
                        break;
                    }
                }
            }
        }

        if(!isAuthorized) {
            throw new UnauthorizedException("You are not allowed to retrieve this module.");
        }
    }

    @Override
    public void checkAdminOnly(Collection<GrantedAuthority> authorities) {
        boolean isAuthorized = false;

        if(authorities != null && !authorities.isEmpty()) {
            for (GrantedAuthority authority: authorities) {
                if(authority.getAuthority().contains(roleManagementConfig.getAdminRole())) {
                    isAuthorized = true;
                    break;
                }
            }
        }

        if(!isAuthorized) {
            throw new UnauthorizedException("You are not allowed to retrieve this");
        }
    }

    @Override
    public void checkUserInModuleBusiness(String jwtToken, String businessName) {
        // Call the profile-service to check if the user is in the right business
        var httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + jwtToken);
        var entity = new HttpEntity<String>(httpHeaders);

        try {
            ResponseEntity<Boolean> response = restTemplate.exchange(
                    roleManagementConfig.getBusinessCheckUri() + businessName, HttpMethod.GET, entity, Boolean.class);

            if(!response.getStatusCode().isError() && response.getBody() != null) {
                if(response.getBody().equals(false)) {
                    throw new UnauthorizedException("You are not allowed to retrieve this");
                }
            } else {
                // the check failed for some reason so you can't be sure the user is allowed to retrieve it.
                throw new UnauthorizedException("You are not allowed to retrieve this");
            }
        } catch (RestClientException e) {
            e.printStackTrace();
            // the check failed for some reason so you can't be sure the user is allowed to retrieve it.
            throw new UnauthorizedException("You are not allowed to retrieve this");
        }
    }
}
