package nl.suitless.moduleservice.Services.Module;

import nl.suitless.moduleservice.Domain.Entities.AuthorizationConfig;
import nl.suitless.moduleservice.Domain.Entities.ModuleReadOnly;
import nl.suitless.moduleservice.Domain.Entities.Node;
import nl.suitless.moduleservice.Domain.Entities.Module;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleNotFoundException;
import nl.suitless.moduleservice.Domain.Exceptions.UnauthorizedException;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleAlreadyExistException;
import org.springframework.data.domain.Page;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

public interface IModuleService {
    /**
     * Gets module based on given id.
     * Private modules are only obtainable for authorized users.
     * @param id of the module you want to find
     * @param authConfig configuration to check the authorizations
     * @return found module
     * @throws ModuleNotFoundException if module with given id is not found in our system
     * @throws UnauthorizedException if you're not allowed to obtain the module
     */
    Module getModuleById(String id, AuthorizationConfig authConfig);

    /**
     * Gets latest non-exposed version of the module based on id given of any version of the module.
     * @param id of one of the module versions you want to find
     * @throws ModuleNotFoundException if the module with given <b>Id</b> is not found in our system
     * or if the module does not have at least one non-exposed version
     */
    Module getLatestNonExposedModuleById(String id);

    /**
     * Gets all modules based on a given name and owner.
     * It will also check if the user is in the correct business.
     * Private modules are only obtainable for authorized users.
     * @param name of the module you want to find
     * @param version of the module you want to find
     * @param owner of the module you want to find
     * @param authConfig configuration to check the authorizations
     * @return found module
     * @throws ModuleNotFoundException if module with given name is not found in our system
     * @throws UnauthorizedException if you're not allowed to obtain the module
     */
    Module getModuleByName(String name, int version, String owner, AuthorizationConfig authConfig);

    /**
     * Gets the latest module based on a given name and owner.
     * It will also check if the user is in the correct business.
     * Private modules are only obtainable for authorized users.
     * @param name of the module you want to find
     * @param owner of the module you want to find
     * @param authConfig configuration to check the authorizations
     * @return found module
     * @throws ModuleNotFoundException if module with given name is not found in our system
     * @throws UnauthorizedException if you're not allowed to obtain the module
     */
    Module getLatestModuleByName(String name, String owner, AuthorizationConfig authConfig);

    /**
     * Gets all modules in the database that are public or public and private from a specific owner.
     * It will also check if the user is in the correct business.
     * Private modules are only obtainable for authorized users.
     * @param version of the modules you want to find
     * @param publicAndPrivate to get only public or public and private modules
     * @param owner of the modules you want to find
     * @param page pagination page you want to retrieve
     * @param size of the page
     * @param authConfig configuration to check the authorizations
     * @return all modules found
     * @throws UnauthorizedException if you're not allowed to obtain the modules
     */
    Page<ModuleReadOnly> getModules(int version, boolean publicAndPrivate, String owner, int page, int size,
                                    AuthorizationConfig authConfig);

    /**
     * Gets all latest modules in the database that are public or public and private.
     * Only allowed for admins.
     * @param publicAndPrivate to get only public or public and private modules
     * @param page pagination page you want to retrieve
     * @param size of the page
     * @param authConfig configuration to check the authorizations
     * @return all modules found
     * @throws UnauthorizedException if you are not allowed to obtain the modules
     */
    Page<ModuleReadOnly> getAllLatestModules(boolean publicAndPrivate, int page, int size, AuthorizationConfig authConfig);

    /**
     * Gets latest modules in the database that are public or public and private from a specific owner.
     * It will also check if the user is in the correct business.
     * Private modules are only obtainable for authorized users.
     * @param publicAndPrivate to get only public or public and private modules
     * @param owner of the modules you want to find
     * @param page pagination page you want to retrieve
     * @param size of the page
     * @param authConfig configuration to check the authorizations
     * @return all modules found
     * @throws UnauthorizedException if you're not allowed to obtain the modules
     */
    Page<ModuleReadOnly> getLatestModules(boolean publicAndPrivate, String owner, int page, int size,
                                          AuthorizationConfig authConfig);

    /**
     * Gets all modules by name in the database that are public and/or private from a specific owner.
     * It will also check if the user is in the correct business.
     * Private modules are only obtainable for authorized users.
     * @param publicAndPrivate to get only public and/or private modules.
     * @param owner of the modules you want to find
     * @param name of the modules you want to find
     * @param page pagination page you want to retrieve
     * @param size of the page
     * @param authConfig configuration to check the authorizations
     * @return all modules found
     * @throws UnauthorizedException if you're not allowed to obtain the module
     */
    Page<ModuleReadOnly> getModulesByName(boolean publicAndPrivate, String owner, String name, int page, int size,
                                          AuthorizationConfig authConfig);

    /**
     * Gets all modules by name in the database.
     * Only allowed for admins.
     * @param name of the modules you want to find
     * @param page pagination page you want to retrieve
     * @param size of the page
     * @param authConfig configuration to check the authorizations
     * @return all modules found
     * @throws UnauthorizedException if you're not allowed to obtain the module
     */
    Page<ModuleReadOnly> getAllModulesByName(String name, int page, int size, AuthorizationConfig authConfig);

    /**
     * Create a module with the given parameters.
     * It will also check if the user is in the correct business and authorized.
     * @param name you want to give the module
     * @param description of the module
     * @param disclaimer of the module
     * @param pdfDisclaimer of the module
     * @param version of the module
     * @param editorVersion of the module
     * @param image of the module as visual representation
     * @param tags that belong to the module
     * @param nodes that belong to the module, created by the front-end module builder
     * @param isPublic to set the module private or public
     * @param owner of the module you want to create
     * @param authConfig configuration to check the authorizations
     * @throws ModuleAlreadyExistException if the module with the same name, version and owner already exists
     * @throws UnauthorizedException if you're not allowed to obtain the module
     * @return the created module
     */
    Module createModule(String name, String description, String disclaimer, String pdfDisclaimer, int version,
                        int editorVersion, String image, List<String> tags, List<Node> nodes, boolean isPublic,
                        String owner, AuthorizationConfig authConfig);

    /**
     * Update a module with the given parameters.
     * It will also check if the user is in the correct business and authorized.
     * @param id of the module you want to update
     * @param name of the module you're updating
     * @param description of the module
     * @param disclaimer of the module
     * @param pdfDisclaimer of the module
     * @param version of the module
     * @param editorVersion of the module
     * @param image of the module as visual representation
     * @param tags that belong to the module
     * @param nodes inside the module
     * @param isPublic to set the module private or public
     * @param owner of the module you want to create
     * @param authConfig configuration to check the authorizations
     * @throws UnauthorizedException if you're not allowed to obtain the module
     * @return the updated module
     */
    Module updateModule(String id, String name, String description, String disclaimer, String pdfDisclaimer, int version,
                        int editorVersion, String image, List<String> tags, List<Node> nodes, boolean isPublic, String owner,
                        AuthorizationConfig authConfig);

    /**
     * Delete module with the given id.
     * It will also check if the user is in the correct business and authorized.
     * @param id of the module to delete
     * @param authConfig configuration to check the authorizations
     * @throws ModuleNotFoundException if the module could not be found in the system
     * @throws UnauthorizedException if you're not allowed to obtain the module
     */
    void deleteModule(String id, AuthorizationConfig authConfig);

    /**
     * Restores a module from the bin back to the main database.
     * It will also check if the user is in the correct business and authorized.
     * @param id of the module that has been deleted
     * @param authConfig configuration to check the authorizations
     * @throws ModuleNotFoundException if the module could not be found in the system
     * @throws UnauthorizedException if you're not allowed to obtain the module
     */
    Module restoreModule(String id, AuthorizationConfig authConfig);

    /**
     * Checks if the module exists in the system.
     * It will also check if the user is authorized.
     * @param id of the module
     * @param authConfig configuration to check the authorizations
     * @return true or false if the module exists yes or no
     * @throws UnauthorizedException if you're not allowed to obtain the module
     */
    boolean moduleExists(String id, AuthorizationConfig authConfig);
}
