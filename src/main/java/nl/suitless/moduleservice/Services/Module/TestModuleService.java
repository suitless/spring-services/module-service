package nl.suitless.moduleservice.Services.Module;

import nl.suitless.moduleservice.Data.testRepositories.IModuleTestRepository;
import nl.suitless.moduleservice.Domain.Entities.Module;
import nl.suitless.moduleservice.Domain.Entities.Node;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleAlreadyExistException;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestModuleService implements ITestModuleService {
    private IModuleTestRepository moduleRepository;

    @Autowired
    public TestModuleService(IModuleTestRepository moduleRepository){ this.moduleRepository = moduleRepository; }

    @Override
    public Module getModuleById(String id) {
        return moduleRepository.findById(id)
                .orElseThrow(() -> new ModuleNotFoundException("module with id: " + id + " Not found"));
    }

    @Override
    public Module createModule(String name, String description, String disclaimer, String pdfDisclaimer, int version, int editorVersion, String image, List<String> tags,
                               List<Node> nodes, boolean isPublic, String owner) {
        checkIfModuleAlreadyExists(name);

        var module = new Module(name, description, disclaimer, pdfDisclaimer, version, editorVersion, image, tags, nodes, isPublic, owner);
        return moduleRepository.save(module);
    }

    //region Generic method
    private void checkIfModuleAlreadyExists(String name) {
        if(moduleRepository.findByName(name).isPresent()){
            throw new ModuleAlreadyExistException("Module with name: " + name + " already exists");
        }
    }
    //endregion
}
