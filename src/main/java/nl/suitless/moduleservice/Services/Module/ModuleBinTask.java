package nl.suitless.moduleservice.Services.Module;

import nl.suitless.moduleservice.Data.Repositories.IBinModuleRepository;
import nl.suitless.moduleservice.Domain.Entities.BinModule;
import org.joda.time.DateTime;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ModuleBinTask {
    private final IBinModuleRepository binModuleRepository;

    public ModuleBinTask(IBinModuleRepository binModuleRepository) {
        this.binModuleRepository = binModuleRepository;
    }

    @Scheduled(cron = "0 0 1 * * ?", zone = "Europe/Amsterdam")
    private void cleanBinRepository() {
        System.out.println("Cleaning up the bin repository!! Shvoooooooooo");

        Iterable<BinModule> binModules = binModuleRepository.findAll();
        binModules.forEach(module -> {
            if(DateTime.now().isAfter(new DateTime(module.getExpireDate()))) {
                binModuleRepository.delete(module);
            }
        });
    }
}
