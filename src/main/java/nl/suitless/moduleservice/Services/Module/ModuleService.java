package nl.suitless.moduleservice.Services.Module;

import nl.suitless.moduleservice.Config.BinConfig.BinConfig;
import nl.suitless.moduleservice.Data.Repositories.IBinModuleRepository;
import nl.suitless.moduleservice.Data.Repositories.IModuleRepository;
import nl.suitless.moduleservice.Domain.Entities.*;
import nl.suitless.moduleservice.Domain.Entities.Module;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleAlreadyExistException;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleNotFoundException;
import nl.suitless.moduleservice.Domain.Exceptions.VersionConflictException;
import nl.suitless.moduleservice.Services.Authorization.IAuthorizationService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Supplier;

@Service
public class ModuleService implements IModuleService {
    private IModuleRepository moduleRepository;
    private IBinModuleRepository binModuleRepository;
    private IAuthorizationService authService;
    private BinConfig binConfig;

    @Autowired
    public ModuleService(IModuleRepository moduleRepository,
                         IBinModuleRepository binModuleRepository,
                         IAuthorizationService authService, BinConfig binConfig) {
        this.moduleRepository = moduleRepository;
        this.binModuleRepository = binModuleRepository;
        this.authService = authService;
        this.binConfig = binConfig;
    }

    @Override
    public Module getModuleById(String id, AuthorizationConfig authConfig) {
        return getModuleByIdIfPresent(id, authConfig.getAuthorities());
    }

    @Override
    public Module getLatestNonExposedModuleById(String id) {
        var foundModule = moduleRepository.findById(id).orElseThrow(() ->
                new ModuleNotFoundException("Module with id: " + id + " not found"));

        return moduleRepository.findFirstByNameAndOwnerAndExposedIsTrueOrderByVersionDesc(foundModule.getName(), foundModule.getOwner()).orElseThrow(() ->
                new ModuleNotFoundException("Latest non-exposed module could not be found"));
    }

    @Override
    public Module getModuleByName(String name, int version, String owner, AuthorizationConfig authConfig) {
        Supplier<Module> getSub = () -> moduleRepository.findByNameAndVersionAndOwner(name, version, owner).orElseThrow(() ->
                new ModuleNotFoundException("Module with name: " + name + ", v: " + version + " not found."));
        return getModule(authConfig, getSub);
    }

    @Override
    public Module getLatestModuleByName(String name, String owner, AuthorizationConfig authConfig) {
        Supplier<Module> getSub = () -> moduleRepository.findByNameAndOwnerOrderByCreatedAtDesc(name, owner).orElseThrow(() ->
                new ModuleNotFoundException("Can't find any module with name: " + name + " in the system."));
        return getModule(authConfig, getSub);
    }

    @Override
    public Page<ModuleReadOnly> getModules(int version, boolean publicAndPrivate, String owner, int page, int size,
                                           AuthorizationConfig authConfig) {
        Pageable paging = PageRequest.of(page, size);
        Supplier<Page<ModuleReadOnly>> privateGetSup = () ->
                moduleRepository.findByVersionAndOwnerOrderByVersionDesc(version, owner, paging);
        Supplier<Page<ModuleReadOnly>> getSup = () ->
                moduleRepository.findByVersionAndOwnerAndExposedIsTrueOrderByVersionDesc(version, owner, paging);
        return getModulesReadOnly(publicAndPrivate, authConfig, privateGetSup, getSup);
    }

    @Override
    public Page<ModuleReadOnly> getAllLatestModules(boolean publicAndPrivate, int page, int size,
                                                    AuthorizationConfig authConfig) {
        authService.checkAdminOnly(authConfig.getAuthorities());

        Pageable paging = PageRequest.of(page, size);
        return moduleRepository.findAllLatestModules(publicAndPrivate, paging);
    }

    @Override
    public Page<ModuleReadOnly> getLatestModules(boolean publicAndPrivate, String owner, int page, int size,
                                                 AuthorizationConfig authConfig) {
        Pageable paging = PageRequest.of(page, size);
        Supplier<Page<ModuleReadOnly>> privateGetSup = () ->
                moduleRepository.findLatestModulesPrivateByOwner(owner, paging);
        Supplier<Page<ModuleReadOnly>> getSup = () ->
                moduleRepository.findLatestModulesByOwner(owner, paging);
        return getModulesReadOnly(publicAndPrivate, authConfig, privateGetSup, getSup);
    }

    @Override
    public Page<ModuleReadOnly> getModulesByName(boolean publicAndPrivate, String owner, String name, int page, int size,
                                                 AuthorizationConfig authConfig) {
        Pageable paging = PageRequest.of(page, size);
        Supplier<Page<ModuleReadOnly>> privateGetSup = () ->
                moduleRepository.findByNameAndOwnerOrderByCreatedAtDesc(name, owner, paging);
        Supplier<Page<ModuleReadOnly>> getSup = () ->
                moduleRepository.findByNameAndOwnerAndExposedIsTrueOrderByCreatedAtDesc(name, owner, paging);
        return getModulesReadOnly(publicAndPrivate, authConfig, privateGetSup, getSup);
    }

    @Override
    public Page<ModuleReadOnly> getAllModulesByName(String name, int page, int size, AuthorizationConfig authConfig) {
        authService.checkAdminOnly(authConfig.getAuthorities());

        Pageable paging = PageRequest.of(page, size);
        return moduleRepository.findByNameOrderByCreatedAtDesc(name, paging);
    }

    @Override
    public Module createModule(String name, String description, String disclaimer, String pdfDisclaimer,
                               int version, int editorVersion, String image, List<String> tags, List<Node> nodes, boolean isPublic,
                               String owner, AuthorizationConfig authConfig) {
        authService.check(authConfig.getAuthorities());
        authService.checkUserInModuleBusiness(authConfig.getJwtToken(), owner);

        moduleRepository.findByNameAndOwner(name, owner).ifPresent(module -> {
                throw new ModuleAlreadyExistException("Module with name: " + name + " already exists, change the name or update the existing module.");
        });

        var module = new Module(name, description, disclaimer, pdfDisclaimer, version, editorVersion, image, tags, nodes, isPublic, owner);
        return moduleRepository.save(module);
    }

    @Override
    public Module updateModule(String id, String name, String description, String disclaimer, String pdfDisclaimer,
                               int version, int editorVersion, String image, List<String> tags, List<Node> nodes, boolean isPublic,
                               String owner, AuthorizationConfig authConfig) {
        authService.checkUserInModuleBusiness(authConfig.getJwtToken(), owner);

        moduleRepository.findByNameAndVersionAndOwner(name, version, owner).ifPresent(module -> {
            throw new VersionConflictException("Module with version: " + version + " already exists, please increase the version number.");
        });

        var module = getModuleByIdIfPresent(id, authConfig.getAuthorities());
        module.setName(name);
        module.setDescription(description);
        module.setDisclaimer(disclaimer);
        module.setPdfDisclaimer(pdfDisclaimer);
        module.setVersion(version);
        module.setEditorVersion(editorVersion);
        module.setImage(image);
        module.setTags(tags);
        module.setNodes(nodes);
        module.setId(null);
        module.setExposed(isPublic);
        return moduleRepository.save(module);
    }

    @Override
    public void deleteModule(String id, AuthorizationConfig authConfig) {
        var module = getModuleByIdIfPresent(id, authConfig.getAuthorities());
        authService.checkUserInModuleBusiness(authConfig.getJwtToken(), module.getOwner());

        // Move module to the bin
        var daysExpired = DateTime.now().plusDays(binConfig.getDaysExpired()).toDate();
        var binModule = new BinModule(module, daysExpired);
        binModuleRepository.save(binModule);
        moduleRepository.delete(module);
    }

    @Override
    public Module restoreModule(String id, AuthorizationConfig authConfig) {
        var binModule = binModuleRepository.findById(id).orElseThrow(() ->
                new ModuleNotFoundException("Module with id: " + id + " not found in the bin. Might be deleted already."));

        if(!binModule.isExposed()) {
            authService.check(authConfig.getAuthorities());
        }

        authService.checkUserInModuleBusiness(authConfig.getJwtToken(), binModule.getOwner());

        // Move module out of the bin
        var module = new Module(binModule);
        moduleRepository.save(module);
        binModuleRepository.delete(binModule);
        return module;
    }

    @Override
    public boolean moduleExists(String id, AuthorizationConfig authConfig) {
        Optional<Module> foundModule = moduleRepository.findById(id);

        boolean moduleExists = foundModule.isPresent();
        if (moduleExists && foundModule.get().isExposed()) {
           authService.check(authConfig.getAuthorities());
        }

        return moduleExists;
    }

    //region Generic methods

    private Module getModuleByIdIfPresent(String id, Collection<GrantedAuthority> authorities) {
        var foundModule = moduleRepository.findById(id).orElseThrow(() ->
                new ModuleNotFoundException("Module with id: " + id + " not found"));

        if(!foundModule.isExposed()) {
            authService.check(authorities);
        }

        return foundModule;
    }

    /**
     * Gets the read-only modules correctly by checking if you are admin when getting private modules
     * and checking if you are in the business.
     * @param publicAndPrivate to get only public or public and private modules
     * @param authConfig to check the roles of the user and if the user is in the correct business
     * @param privateGetSup function to get the admin read-only modules
     * @param getSup function to get the non-admin read-only modules
     * @return list of read-only modules
     */
    private Page<ModuleReadOnly> getModulesReadOnly(boolean publicAndPrivate, AuthorizationConfig authConfig,
                                                    Supplier<Page<ModuleReadOnly>> privateGetSup,
                                                    Supplier<Page<ModuleReadOnly>> getSup) {
        Page<ModuleReadOnly> modules;
        if(publicAndPrivate) {
            authService.check(authConfig.getAuthorities());
            modules = privateGetSup.get();
        } else {
            modules = getSup.get();
        }

        // if the there are no modules found, no need to check this
        if(modules.hasContent() && modules.getContent().size() > 0) {
            authService.checkUserInModuleBusiness(authConfig.getJwtToken(), modules.getContent().get(0).getOwner());
        }
        return modules;
    }

    /**
     * Gets a module correctly by checking if you are admin when getting a private module
     * and checking if you are in the business.
     * @param authConfig to check the roles of the user and if the user is in the correct business
     * @param getSup function to get the module
     * @return found module
     */
    private Module getModule(AuthorizationConfig authConfig, Supplier<Module> getSup) {
        var foundModule = getSup.get();

        if(!foundModule.isExposed()) {
            authService.check(authConfig.getAuthorities());
        }
        authService.checkUserInModuleBusiness(authConfig.getJwtToken(), foundModule.getOwner());

        return foundModule;
    }

    //endregion
}