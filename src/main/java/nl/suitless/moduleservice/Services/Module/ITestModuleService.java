package nl.suitless.moduleservice.Services.Module;

import nl.suitless.moduleservice.Domain.Entities.Module;
import nl.suitless.moduleservice.Domain.Entities.Node;
import nl.suitless.moduleservice.Domain.Exceptions.ModuleNotFoundException;

import java.util.List;

/**
 * Test Module Service used for interaction with the test module database.
 * The test module database can be used for a complete separate testing system.
 * For instance to use for e2e testing of front-end services.
 */
public interface ITestModuleService {
    /**
     * Gets test module on id given
     * @param id of the test module you want to find
     * @return found module
     * @throws ModuleNotFoundException if the test module with given <b>Id</b> is not found in our system
     */
    Module getModuleById(String id);

    /**
     * Create a test module with the given parameters
     * @param name the name you want to give the module
     * @param description the description of the module
     * @param disclaimer of the module
     * @param pdfDisclaimer of the module
     * @param version of the module
     * @param editorVersion of the module
     * @param image of the module as visual representation
     * @param tags that belong to the module
     * @param nodes that belong to the module, created by the front-end module builder
     * @param isPublic to set the module private or public
     * @param owner of the module you want to create
     * @return the created module
     */
    Module createModule(String name, String description, String disclaimer, String pdfDisclaimer, int version, int editorVersion, String image, 
                        List<String> tags, List<Node> nodes, boolean isPublic, String owner);
}
