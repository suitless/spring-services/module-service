package nl.suitless.moduleservice.Services.Utils;

import nl.suitless.moduleservice.Domain.Entities.ModuleReadOnly;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public final class PageListTranslator {
    private PageListTranslator() {}

    public static List<ModuleReadOnly> PageToList(Page<ModuleReadOnly> page) {
        if(page.hasContent()) {
            return new ArrayList<>(page.getContent());
        } else {
            return new ArrayList<>();
        }
    }
}
