package nl.suitless.moduleservice.Web.Wrappers;

import javax.validation.constraints.NotNull;

public class AccesRequestModel {
    @NotNull
    private boolean exposed;

    public AccesRequestModel() {
    }

    public boolean isExposed() {
        return exposed;
    }

    public void setExposed(boolean exposed) {
        this.exposed = exposed;
    }
}
