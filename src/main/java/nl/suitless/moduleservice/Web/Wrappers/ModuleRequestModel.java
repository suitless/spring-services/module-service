package nl.suitless.moduleservice.Web.Wrappers;

import nl.suitless.moduleservice.Domain.Entities.Node;

import javax.validation.constraints.NotNull;
import java.util.List;

public class ModuleRequestModel {
    @NotNull
    private String name;
    @NotNull
    private String description;
    @NotNull
    private String disclaimer;
    @NotNull
    private String pdfDisclaimer;
    @NotNull
    private int version;
    @NotNull
    private int editorVersion;
    @NotNull
    private String image;
    @NotNull
    private List<String> tags;
    @NotNull
    private List<Node> nodes;

    private boolean exposed = false;
    @NotNull
    private String owner;

    public ModuleRequestModel() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

        public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getPdfDisclaimer() {
        return pdfDisclaimer;
    }

    public void setPdfDisclaimer(String pdfDisclaimer) {
        this.pdfDisclaimer = pdfDisclaimer;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getEditorVersion() {
        return editorVersion;
    }

    public void setEditorVersion(int editorVersion) {
        this.editorVersion = editorVersion;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean isExposed() {
        return exposed;
    }

    public void setExposed(boolean exposed) {
        this.exposed = exposed;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
