package nl.suitless.moduleservice.Web.Controllers;

import nl.suitless.moduleservice.Services.Module.ITestModuleService;
import nl.suitless.moduleservice.Web.HateosResources.ModuleResource;
import nl.suitless.moduleservice.Web.Wrappers.ModuleRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Controller
@RequestMapping("/test")
public class TestModuleController {
    private ITestModuleService moduleService;

    @Autowired
    public TestModuleController(ITestModuleService moduleService) { this.moduleService = moduleService; }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ModuleResource> getModuleById(@PathVariable("id") String id) {
        var moduleResource = new ModuleResource(moduleService.getModuleById(id));
        moduleResource.add(linkTo(methodOn(TestModuleController.class).getModuleById(id)).withSelfRel());

        return new ResponseEntity<>(moduleResource, HttpStatus.OK);
    }

    @PostMapping(path = "/")
    public ResponseEntity<ModuleResource> createModule(@Valid @RequestBody ModuleRequestModel requestModel) {
        var moduleResource = new ModuleResource(moduleService.createModule(
                requestModel.getName(), requestModel.getDescription(), requestModel.getDisclaimer(), requestModel.getPdfDisclaimer(), requestModel.getVersion(),
                requestModel.getEditorVersion(), requestModel.getImage(), requestModel.getTags(), requestModel.getNodes(), requestModel.isExposed(), requestModel.getOwner()));

        moduleResource.add(linkTo(methodOn(TestModuleController.class).createModule(requestModel)).withSelfRel());
        moduleResource.add(linkTo(methodOn(TestModuleController.class).getModuleById(moduleResource.getModule().getId())).withRel("Get"));

        return new ResponseEntity<>(moduleResource, HttpStatus.OK);
    }
}
