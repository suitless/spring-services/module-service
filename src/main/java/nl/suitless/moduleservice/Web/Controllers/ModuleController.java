package nl.suitless.moduleservice.Web.Controllers;

import nl.suitless.moduleservice.Domain.Entities.AuthorizationConfig;
import nl.suitless.moduleservice.Domain.Entities.ModuleReadOnly;
import nl.suitless.moduleservice.Services.Module.IModuleService;
import nl.suitless.moduleservice.Services.Utils.PageListTranslator;
import nl.suitless.moduleservice.Web.HateosResources.ModuleResource;
import nl.suitless.moduleservice.Web.HateosResources.ModulesResource;
import nl.suitless.moduleservice.Web.Wrappers.ModuleRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Controller
@RequestMapping("")
public class ModuleController {
    private final IModuleService moduleService;

    @Autowired
    public ModuleController(IModuleService moduleService) {
        this.moduleService = moduleService;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<ModuleResource> getModuleById(Principal principal, @PathVariable("id") String id) {
        ModuleResource moduleResource = new ModuleResource(moduleService.getModuleById(id, new AuthorizationConfig(principal)));

        moduleResource.add(linkTo(methodOn(ModuleController.class).getModuleById(principal, id)).withSelfRel());
        moduleResource.add(linkTo(methodOn(ModuleController.class).deleteModule(principal, id)).withRel("Delete"));

        return new ResponseEntity<>(moduleResource, HttpStatus.OK);
    }

    @GetMapping(path = "/latest/{id}")
    public ResponseEntity<ModuleResource> getLatestNonExposedModuleById(@PathVariable("id") String id) {
        ModuleResource moduleResource = new ModuleResource(moduleService.getLatestNonExposedModuleById(id));

        return new ResponseEntity<>(moduleResource, HttpStatus.OK);
    }

    @GetMapping(path = "/{owner}/name/{name}/v/{version}")
    public ResponseEntity<ModuleResource> getModuleByName(Principal principal, @PathVariable("owner") String owner,
                                                          @PathVariable("name") String name, @PathVariable("version") int version) {
        var foundModule = moduleService.getModuleByName(name, version, owner, new AuthorizationConfig(principal));
        var moduleResource = new ModuleResource(foundModule);

        moduleResource.add(linkTo(methodOn(ModuleController.class).getModuleById(principal, foundModule.getId())).withSelfRel());
        moduleResource.add(linkTo(methodOn(ModuleController.class).deleteModule(principal, foundModule.getId())).withRel("Delete"));

        return new ResponseEntity<>(moduleResource, HttpStatus.OK);
    }

    @GetMapping(path = "/{owner}/name/{name}/latest")
    public ResponseEntity<ModuleResource> getLatestModuleByName(Principal principal, @PathVariable("owner") String owner,
                                                                @PathVariable("name") String name){
        var foundModule = moduleService.getLatestModuleByName(name, owner, new AuthorizationConfig(principal));
        var moduleResource = new ModuleResource(foundModule);

        moduleResource.add(linkTo(methodOn(ModuleController.class).getLatestModuleByName(principal, owner, name)).withSelfRel());
        moduleResource.add(linkTo(methodOn(ModuleController.class).deleteModule(principal, foundModule.getId())).withRel("Delete"));

        return new ResponseEntity<>(moduleResource, HttpStatus.OK);
    }

    @GetMapping(path = {"/{owner}/v/{version}", "/{owner}/v/{version}/private"}, params = { "page", "size" })
    public ResponseEntity<ModulesResource> getModules(Principal principal, @PathVariable("owner") String owner,
                                                      @PathVariable("version") int version, @RequestParam("page") int page,
                                                      @RequestParam("size") int size, HttpServletRequest request) {
        Page<ModuleReadOnly> result = moduleService.getModules(version, request.getRequestURI().contains("private"),
                owner, page, size, new AuthorizationConfig(principal));
        var resource = new ModulesResource(PageListTranslator.PageToList(result), result.getTotalElements());

        resource.add(linkTo(methodOn(ModuleController.class).getModules(principal, owner, version, page, size, request)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @GetMapping(path = {"/all/latest", "/all/latest/private"}, params = { "page", "size" })
    public ResponseEntity<ModulesResource> getAllLatestModules(Principal principal, @RequestParam("page") int page,
                                                               @RequestParam("size") int size, HttpServletRequest request) {
        Page<ModuleReadOnly> result = moduleService.getAllLatestModules(request.getRequestURI().contains("private"),
                page, size, new AuthorizationConfig(principal));
        var resource = new ModulesResource(PageListTranslator.PageToList(result), result.getTotalElements());

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @GetMapping(path = {"/{owner}/latest", "/{owner}/latest/private"}, params = { "page", "size" })
    public ResponseEntity<ModulesResource> getLatestModules(Principal principal, @PathVariable("owner") String owner,
                                                            @RequestParam("page") int page, @RequestParam("size") int size,
                                                            HttpServletRequest request) {
        Page<ModuleReadOnly> result = moduleService.getLatestModules(request.getRequestURI().contains("private"), owner,
                page, size, new AuthorizationConfig(principal));
        var resource = new ModulesResource(PageListTranslator.PageToList(result), result.getTotalElements());

        resource.add(linkTo(methodOn(ModuleController.class)
                .getLatestModules(principal, owner, page, size, request)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @GetMapping(path = {"/{owner}/name/{name}", "/{owner}/name/{name}/private"}, params = { "page", "size" })
    public ResponseEntity<ModulesResource> getModulesByName(Principal principal, @PathVariable("owner") String owner,
                                                           @PathVariable("name") String name, @RequestParam("page") int page,
                                                            @RequestParam("size") int size, HttpServletRequest request) {
        Page<ModuleReadOnly> result = moduleService.getModulesByName(request.getRequestURI().contains("private"), owner,
                name, page, size, new AuthorizationConfig(principal));
        var resource = new ModulesResource(PageListTranslator.PageToList(result), result.getTotalElements());

        resource.add(linkTo(methodOn(ModuleController.class).getModulesByName(principal, owner, name, page, size, request)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @GetMapping(path = "/name/{name}", params = { "page", "size" })
    public ResponseEntity<ModulesResource> getAllModulesByName(Principal principal, @PathVariable("name") String name,
                                                               @RequestParam("page") int page, @RequestParam("size") int size) {
        Page<ModuleReadOnly> result = moduleService.getAllModulesByName(name, page, size, new AuthorizationConfig(principal));
        ModulesResource resource = new ModulesResource(PageListTranslator.PageToList(result), result.getTotalElements());

        resource.add(linkTo(methodOn(ModuleController.class).getAllModulesByName(principal, name, page, size)).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @PostMapping(path = "/")
    public ResponseEntity<ModuleResource> createModule(Principal principal, @Valid @RequestBody ModuleRequestModel requestModel) {
        var createdModule = moduleService.createModule(requestModel.getName(), requestModel.getDescription(),
                requestModel.getDisclaimer(), requestModel.getPdfDisclaimer(), requestModel.getVersion(), requestModel.getEditorVersion(),
                requestModel.getImage(), requestModel.getTags(), requestModel.getNodes(), requestModel.isExposed(),
                requestModel.getOwner(), new AuthorizationConfig(principal));
        var moduleResource = new ModuleResource(createdModule);

        moduleResource.add(linkTo(methodOn(ModuleController.class).createModule(principal, requestModel)).withSelfRel());
        moduleResource.add(linkTo(methodOn(ModuleController.class).deleteModule(principal, createdModule.getId())).withRel("Delete"));
        moduleResource.add(linkTo(methodOn(ModuleController.class).getModuleById(principal, moduleResource.getModule().getId())).withRel("Get"));

        return new ResponseEntity<>(moduleResource, HttpStatus.OK);
    }

    // this is called clean code, shut up virgins :)
    @PutMapping(path = "/{id}")
    public ResponseEntity<ModuleResource> updateModule(Principal principal, @PathVariable("id") String id,
                                                       @Valid @RequestBody ModuleRequestModel requestModel) {
        var moduleResource = new ModuleResource(moduleService.updateModule(id, requestModel.getName(),
                requestModel.getDescription(), requestModel.getDisclaimer(), requestModel.getPdfDisclaimer(),
                requestModel.getVersion(), requestModel.getEditorVersion(), requestModel.getImage(), requestModel.getTags(), requestModel.getNodes(),
                requestModel.isExposed(), requestModel.getOwner(), new AuthorizationConfig(principal)));

        moduleResource.add(linkTo(methodOn(ModuleController.class).updateModule(principal, id, requestModel)).withSelfRel());
        moduleResource.add(linkTo(methodOn(ModuleController.class).getModuleById(principal, id)).withRel("Get"));
        moduleResource.add(linkTo(methodOn(ModuleController.class).deleteModule(principal, id)).withRel("Delete"));

        return new ResponseEntity<>(moduleResource, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteModule(Principal principal, @PathVariable("id") String id) {
        moduleService.deleteModule(id, new AuthorizationConfig(principal));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/restore/{id}")
    public ResponseEntity<ModuleResource> restoreModule(Principal principal, @PathVariable("id") String id) {
        var resource = new ModuleResource(moduleService.restoreModule(id, new AuthorizationConfig(principal)));
        resource.add(linkTo(methodOn(ModuleController.class).restoreModule(principal, id)).withSelfRel());

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @GetMapping(path = "/exists/{id}")
    public ResponseEntity<Boolean> checkIfModuleExists(Principal principal, @PathVariable("id") String id) {
        return new ResponseEntity<>(moduleService.moduleExists(id, new AuthorizationConfig(principal)), HttpStatus.OK);
    }
}
