package nl.suitless.moduleservice.Web.HateosResources;

import nl.suitless.moduleservice.Domain.Entities.ModuleReadOnly;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class ModulesResource extends RepresentationModel<ModulesResource> {
    private List<ModuleReadOnly> modules;
    private long totalElements;

    public ModulesResource() {

    }

    public ModulesResource(List<ModuleReadOnly> modules, long totalElements) {
        this.modules = modules;
        this.totalElements = totalElements;
    }

    public List<ModuleReadOnly> getModules() {
        return modules;
    }

    public void setModules(List<ModuleReadOnly> modules) {
        this.modules = modules;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }
}
