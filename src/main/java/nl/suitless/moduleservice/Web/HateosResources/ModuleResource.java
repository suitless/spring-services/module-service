package nl.suitless.moduleservice.Web.HateosResources;

import nl.suitless.moduleservice.Domain.Entities.Module;
import org.springframework.hateoas.RepresentationModel;

public class ModuleResource extends RepresentationModel<ModuleResource> {
    private Module module;

    public ModuleResource() {

    }

    public ModuleResource(Module module) {
        this.module = module;
    }

    public Module getModule() {
        return module;
    }
}
