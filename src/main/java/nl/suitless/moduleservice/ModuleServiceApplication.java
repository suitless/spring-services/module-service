package nl.suitless.moduleservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ModuleServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModuleServiceApplication.class, args);
	}

}
