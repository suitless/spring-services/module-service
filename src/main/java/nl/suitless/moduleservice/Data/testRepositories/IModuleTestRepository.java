package nl.suitless.moduleservice.Data.testRepositories;

import nl.suitless.moduleservice.Domain.Entities.Module;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface IModuleTestRepository extends MongoRepository<Module, Integer> {
    Optional<Module> findById(String id);
    Optional<Module> findByName(String name);
    List<Module> findAllByName(String name);
    List<Module> findAllByNameOrderByCreatedAtDesc(String name);
    Module deleteByName(String name);
    List<Module> deleteAllByName(String name);
}
