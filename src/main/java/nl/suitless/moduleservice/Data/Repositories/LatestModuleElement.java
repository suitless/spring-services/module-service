package nl.suitless.moduleservice.Data.Repositories;

import nl.suitless.moduleservice.Domain.Entities.ModuleReadOnly;

public class LatestModuleElement {
    private String id;
    private int version;
    private ModuleReadOnly doc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public ModuleReadOnly getDoc() {
        return doc;
    }

    public void setDoc(ModuleReadOnly doc) {
        this.doc = doc;
    }
}
