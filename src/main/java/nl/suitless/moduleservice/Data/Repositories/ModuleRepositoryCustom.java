package nl.suitless.moduleservice.Data.Repositories;

import nl.suitless.moduleservice.Domain.Entities.ModuleReadOnly;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ModuleRepositoryCustom {
    /**
     * Finds the latest public modules from a specific owner
     * This is done by grouping the name and get the max version.
     * @param owner of the module
     * @param pageable to get a specific amount of modules
     * @return Modules page
     */
    Page<ModuleReadOnly> findLatestModulesByOwner(String owner, Pageable pageable);

    /**
     * Find the latest private and public modules from a specific owner
     * This is done by grouping the name and get the max version.
     * @param owner of the module
     * @param pageable to get a specific amount of modules
     * @return Modules page
     */
    Page<ModuleReadOnly> findLatestModulesPrivateByOwner(String owner, Pageable pageable);

    /**
     * Find all latest modules from a specific owner
     * This is done by grouping the name and get the max version
     * @param publicAndPrivate to only get public or public and private modules
     * @param pageable to get a specific amount of modules
     * @return Modules page
     */
    Page<ModuleReadOnly> findAllLatestModules(boolean publicAndPrivate, Pageable pageable);
}
