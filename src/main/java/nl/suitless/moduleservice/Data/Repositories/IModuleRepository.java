package nl.suitless.moduleservice.Data.Repositories;

import nl.suitless.moduleservice.Domain.Entities.ModuleReadOnly;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import nl.suitless.moduleservice.Domain.Entities.Module;

import java.util.Optional;

public interface IModuleRepository extends MongoRepository<Module, String>, ModuleRepositoryCustom {
    Optional<Module> findByNameAndVersionAndOwner(String name, int version, String owner);
    Optional<Module> findByNameAndOwner(String name, String owner);

    Page<ModuleReadOnly> findByVersionAndOwnerOrderByVersionDesc(int version, String owner, Pageable pageable);
    Page<ModuleReadOnly> findByVersionAndOwnerAndExposedIsTrueOrderByVersionDesc(int version, String owner, Pageable pageable);

    Optional<Module> findByNameAndOwnerOrderByCreatedAtDesc(String name, String owner);

    Page<ModuleReadOnly> findByNameAndOwnerOrderByCreatedAtDesc(String name, String owner, Pageable pageable);
    Page<ModuleReadOnly> findByNameAndOwnerAndExposedIsTrueOrderByCreatedAtDesc(String name, String owner, Pageable pageable);
    Optional<Module> findFirstByNameAndOwnerAndExposedIsTrueOrderByVersionDesc(String name, String owner);

    Page<ModuleReadOnly> findByNameOrderByCreatedAtDesc(String name, Pageable pageable);
}
