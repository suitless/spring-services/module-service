package nl.suitless.moduleservice.Data.Repositories;

public class TotalElements {
    private long count;

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
