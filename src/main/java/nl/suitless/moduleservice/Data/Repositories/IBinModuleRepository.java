package nl.suitless.moduleservice.Data.Repositories;

import nl.suitless.moduleservice.Domain.Entities.BinModule;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IBinModuleRepository extends MongoRepository<BinModule, String> {
}
