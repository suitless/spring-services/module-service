package nl.suitless.moduleservice.Data.Repositories;

import nl.suitless.moduleservice.Domain.Entities.ModuleReadOnly;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
public class ModuleRepositoryCustomImpl implements ModuleRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public ModuleRepositoryCustomImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Page<ModuleReadOnly> findLatestModulesByOwner(String owner, Pageable pageable) {
        var matchOperation = getOwnerMatchOperation(owner);
        var matchPublicOperation = getPublicMatchOperation();
        var sortOperation = getLatestVersionSortOperation();
        var groupOperation = getLatestVersionGroupOperation();

        var aggregation = Aggregation.newAggregation(matchOperation, matchPublicOperation, sortOperation, groupOperation,
                skip((long) pageable.getPageNumber() * pageable.getPageSize()),
                limit(pageable.getPageSize()));

        return getLatestModules(aggregation, owner, pageable);
    }

    @Override
    public Page<ModuleReadOnly> findLatestModulesPrivateByOwner(String owner, Pageable pageable) {
        var matchOwnerOperation = getOwnerMatchOperation(owner);
        var sortOperation = getLatestVersionSortOperation();
        var groupOperation = getLatestVersionGroupOperation();

        var aggregation = Aggregation.newAggregation(matchOwnerOperation, sortOperation, groupOperation,
                skip((long) pageable.getPageNumber() * pageable.getPageSize()),
                limit(pageable.getPageSize()));

        return getLatestModules(aggregation, owner, pageable);
    }

    private Page<ModuleReadOnly> getLatestModules(Aggregation aggregation, String owner, Pageable pageable) {
        var results = mongoTemplate.aggregate(
                aggregation, "module", LatestModuleElement.class).getMappedResults();

        var total = getTotal(owner);
        var count = total.isEmpty() ? 0 : total.get(0).getCount();
        return new PageImpl<>(results.stream().map(LatestModuleElement::getDoc).collect(Collectors.toList()),
                pageable, count);
    }

    //get the count of the total elements by using the group as count without skip and limiter
    private List<TotalElements> getTotal(String owner) {
        var aggregation = Aggregation.newAggregation(getOwnerMatchOperation(owner), getLatestVersionCountGroupOperation(),
                countGroupOperation());
        return mongoTemplate.aggregate(aggregation, "module", TotalElements.class).getMappedResults();
    }

    @Override
    public Page<ModuleReadOnly> findAllLatestModules(boolean publicAndPrivate, Pageable pageable) {
        var groupOperation = getAllLatestVersionGroupOperation();
        var sortOperation = getLatestVersionSortOperation();
        var matchPublicOperation = getPublicMatchOperation();
        Aggregation aggregation;
        //get the count of the total elements by using the group as count without skip and limiter
        Aggregation totalAggregation;
        if(!publicAndPrivate) {
            aggregation = Aggregation.newAggregation(matchPublicOperation, sortOperation, groupOperation,
                    skip((long) pageable.getPageNumber() * pageable.getPageSize()), limit(pageable.getPageSize()));
            totalAggregation = Aggregation.newAggregation(matchPublicOperation,
                    getLatestVersionCountGroupOperation(), countGroupOperation());
        } else {
            aggregation = Aggregation.newAggregation(sortOperation, groupOperation,
                    skip((long) pageable.getPageNumber() * pageable.getPageSize()), limit(pageable.getPageSize()));
            totalAggregation = Aggregation.newAggregation(getLatestVersionCountGroupOperation(), countGroupOperation());
        }

        var results = mongoTemplate.aggregate(aggregation, "module", LatestModuleElement.class).getMappedResults();
        var total = mongoTemplate.aggregate(totalAggregation, "module", TotalElements.class).getMappedResults();
        var count = total.isEmpty() ? 0 : total.get(0).getCount();
        return new PageImpl<>(results.stream().map(LatestModuleElement::getDoc).collect(Collectors.toList()), pageable, count);
    }

    private MatchOperation getOwnerMatchOperation(String owner) {
        return match(where("owner").is(owner));
    }

    private MatchOperation getPublicMatchOperation() {
        return match(where("exposed").is(true));
    }

    private SortOperation getLatestVersionSortOperation() {
        return sort(Sort.Direction.DESC, "version");
    }

    private GroupOperation getLatestVersionCountGroupOperation() {
        return group("name").max("version").as("version");
    }

    private GroupOperation countGroupOperation() {
        return group().count().as("count");
    }

    private GroupOperation getAllLatestVersionGroupOperation() {
        return  group("owner", "name")
                .max("version").as("version")
                .first("$$ROOT").as("doc");
    }

    private GroupOperation getLatestVersionGroupOperation() {
        return group("name")
                .max("version").as("version")
                .first("$$ROOT").as("doc");
    }
}
