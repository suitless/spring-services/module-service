package nl.suitless.moduleservice.Config.Mongo;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = "nl.suitless.moduleservice.Data.testRepositories",
        mongoTemplateRef = "secondaryMongoTemplate")
public class SecondaryMongoConfig {

}
