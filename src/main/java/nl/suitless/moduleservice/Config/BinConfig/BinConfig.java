package nl.suitless.moduleservice.Config.BinConfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(BinProperties.class)
public class BinConfig {
    private final BinProperties binProperties;

    @Autowired
    public BinConfig(BinProperties binProperties) {
        this.binProperties = binProperties;
    }

    public int getDaysExpired() { return this.binProperties.getDaysExpired(); }
}
