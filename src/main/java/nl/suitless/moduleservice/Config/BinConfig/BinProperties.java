package nl.suitless.moduleservice.Config.BinConfig;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;

@ConfigurationProperties(prefix = "module.bin")
public class BinProperties {
    @NotBlank(message = "expire date must be given in properties")
    @JsonProperty("days-expired")
    private int daysExpired;

    public BinProperties() {
    }

    @NotBlank
    public int getDaysExpired() {
        return daysExpired;
    }

    public void setDaysExpired(int daysExpired) {
        this.daysExpired = daysExpired;
    }
}
