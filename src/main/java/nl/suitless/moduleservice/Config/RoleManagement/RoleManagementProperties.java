package nl.suitless.moduleservice.Config.RoleManagement;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ConfigurationProperties(prefix = "authorization")
public class RoleManagementProperties {
    @NotBlank(message = "Role must be given in properties")
    private String[] roles;

    @NotBlank(message = "Admin role must be given in properties")
    @JsonProperty("admin-role")
    private String adminRole;

    @NotBlank(message = "Business check URI must be given in properties")
    @JsonProperty("business-check-uri")
    private String businessCheckUri;

    public RoleManagementProperties() {
    }

    @NotNull
    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    @NotNull
    public String getAdminRole() {
        return adminRole;
    }

    public void setAdminRole(String adminRole) {
        this.adminRole = adminRole;
    }

    @NotNull
    public String getBusinessCheckUri() {
        return businessCheckUri;
    }

    public void setBusinessCheckUri(String businessCheckUri) {
        this.businessCheckUri = businessCheckUri;
    }
}
